# Welcome to the CentOS Virtualziation SIG

Welcome to the CentOS Virtualization Special Interest Group (Virt-SIG)!

The Virt-SIG aims to deliver a user consumable full stack for virtualization technologies that want to work with the SIG.
This includes delivery, deployment, management, update and patch application (for full lifecycle management) of the
baseline platform when deployed in sync with a technology curated by the Virt-SIG.

The delivery stage would be executed via multiple online, updated RPM repositories to complement ISO based delivery
of install media, livecd/livedvd/livdusb media etc.

The SIG will also attempt to maintain user grade documentation around the technologies we curate, along with represent the
Virt-SIG at CentOS Dojos and related events in community and vendor neutral events.

The SIG will only work with opensource, redistributable software. However some of the code we want to build and ship might
not be mainline accepted as yet, and would be clearly indicated as such.

## Communication Channels

* [CentOS Virtualization mailing list](https://lists.centos.org/hyperkitty/list/virt@lists.centos.org/)
* [CentOS Development mailing list](https://lists.centos.org/hyperkitty/list/devel@lists.centos.org/)
* `#centos-virt` on [Libera Chat](https://libera.chat/)
* Chair (to contact for more information): [Sandro Bonazzola](https://accounts.fedoraproject.org/user/sbonazzo/)

## Meetings

The CentOS Virtualization Special Interest Group will not be holding regular meetings for the time being.
Instead, all discussions and collaborations should take place on our
[mailing list](https://lists.centos.org/hyperkitty/list/virt@lists.centos.org/).

If a meeting is needed for a specific topic, members can request one through the mailing list,
and we will organize it as necessary.

We appreciate your participation and look forward to engaging discussions online.
