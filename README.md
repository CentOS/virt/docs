# Documentation for the Virtualization SIG

Changes are built and published automatically on [sigs.centos.org/virt](https://sigs.centos.org/virt/) a few minutes after they're pushed.

To test changes locally, run:

```bash
podman run --rm -it -p 8000:8000 -v ${PWD}:/docs:z docker.io/squidfunk/mkdocs-material
```

For more info, see <https://sigs.centos.org/guide/documentation/>.
